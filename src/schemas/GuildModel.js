const mongoose = require('mongoose')

module.exports = mongoose.model('GuildModel', new mongoose.Schema({
    id: Number,
    verification:  {
        isSwitched: Boolean
    }
}))
