const { Events, ActivityType } = require('discord.js');
/**
 * client
 */

module.exports = {
	name: Events.ClientReady,
	once: true,
	execute(client) {
		client.user.setPresence({ activities: [{ name: 'tu horario', type: ActivityType.Watching }, { name: `${client.guilds.cache.size} servidores`, type: ActivityType.Watching }] });
		console.log(`Ready! Logged in as ${client.user.tag}`);
	},
};