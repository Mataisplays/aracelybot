const { EmbedBuilder } = require("@discordjs/builders");

module.exports = {
    error: function(cerror, i){
    return new EmbedBuilder()
        .setTitle("Error")
        .setDescription(cerror)
        .setAuthor({ name: i.user.tag, iconURL: i.user.displayAvatarURL()})
        .setColor([250, 105, 74])
    },
    success: function(desc, i){
        return new EmbedBuilder()
        .setTitle("Success")
        .setDescription(desc)
        .setAuthor({ name: i.user.tag, iconURL: i.user.displayAvatarURL()})
        .setColor([75, 204, 82])
    }
}