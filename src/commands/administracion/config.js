const { SlashCommandBuilder, Client } = require("discord.js");
const Discord = require("discord.js")
const UserModel = require('../../schemas/GuildModel')
/**
 * @param {Client} client
 * @param {Discord.CommandInteraction} interaction
 */

module.exports = {
	data: new SlashCommandBuilder()
	.setName('config')
	.setDescription('aronoumen')
	.addSubcommandGroup((group) =>
	  group
		.setName('verification')
		.setDescription('xd')
		.addSubcommand((subcommand) =>
		  subcommand
			.setName('switch')
			.setDescription('Activa o desactiva el sistema')
			.addBooleanOption((option) =>
			  option.setName('switch').setDescription('true = activar, false = desactivar ').setRequired(true)
			)
		)
		.addSubcommand((subcommand) =>
		  subcommand
			.setName('proximamente')
			.setDescription('shh')
			.addUserOption((option) =>
			  option.setName('user').setDescription('user to be banned')
			)
		)
	)
	.addSubcommandGroup((group) =>
	  group
		.setName('proximamente')
		.setDescription('shhh')
		.addSubcommand((subcommand) =>
		  subcommand.setName('lol').setDescription('pipipipipipi')
		)
	),

	async execute(interaction, client) {
		switch(interaction.options.getSubcommandGroup()){
			case "verification":

			switch(interaction.options.getSubcommand()){
				case "switch":
					let Is = interaction.options.getBoolean('switch')
					if(await UserModel.findOne({id: interaction.guild.id})){
						if(await UserModel.findOne({id: interaction.guild.id, verification:{isSwitched:Is}})){
							return interaction.reply({embeds:[client.functions.Embeds.error(`Ya esta establecido en: **${Is ? "Activado":"Desactivado"}**`, interaction)]})
						}
						await UserModel.updateOne({id: interaction.guild.id},{verification:{isSwitched:Is}})
					}else{
						await UserModel.create({id:interaction.guild.id, verification:{isSwitched:Is}})
						if(!Is){
							return interaction.reply({embeds:[client.functions.Embeds.error(`Ya esta establecido en: **${Is ? "Activado":"Desactivado"}**`, interaction)]})
						}
					}
					interaction.reply({embeds:[client.functions.Embeds.success('Se ha activado/desactivado el sistema exitosamente', interaction)]})
					
			}
		}
	},
};
